# Hackmanila Project

This repository is dedicated to the project for the [Hackmanila hackathon](https://hackmanila.com/).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software:

* [Git](https://git-scm.com/)
* [Node.js](https://nodejs.org/en/)
* [React.js](https://reactjs.org/)
* [React Native](https://reactnative.dev/)
* [Expo](https://expo.io/)

### Installing

Here's a step by step series of examples that tell you how to get a development env running:

In your terminal, clone the repository

```
git clone git clone https://dgabrielablay@bitbucket.org/dgabrielablay/hackmanila-project.git
```

Go into that directory

```
cd Hackmanila-project
```

Install dependencies

```
npm install
```

Run the project

```
npm start
```

## Deployment

Should you need to deploy this project in a live system, this command will generate the .apk file for android phones.

```
expo build android
```

## Built With

* [React.js](https://reactjs.org/) - Frontend client used
* [Expo](https://expo.io/) - Generating .apk file

## Versioning

We use [Git](https://git-scm.com/) for versioning.

## Authors

* **Don Gabriel Ablay** - *Initial work* - [dgda](https://github.com/dgda)

## Acknowledgments

* Hat tip to [StackOverflow](https://stackoverflow.com/) and its users for the debugging and tutorial on many topics to build this system.